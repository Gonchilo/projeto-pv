﻿using Microsoft.AspNetCore.Http;
using Projeto_PV.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.ViewModels
{
    public class SerieViewModel
    {
        [Required(ErrorMessage = "Please put the title of the serie")]
        public String Title { get; set; }

        public String Description { get; set; }

        public String Episodes { get; set; }

        public String Seasons { get; set; }

        public List<Review> reviews { get; set; }

        [Display(Name = "Upload Image")]
        [Required(ErrorMessage = "Please choose a cover image")]
        public IFormFile CoverImage { get; set; }
    }
}
