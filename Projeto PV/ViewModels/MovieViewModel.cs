﻿using Microsoft.AspNetCore.Http;
using Projeto_PV.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.ViewModels
{
    public class MovieViewModel
    {
        [Required(ErrorMessage = "Please put the title of the movie")]
        public String Title { get; set; }

        [Required(ErrorMessage = "Please write the description of the movie")]
        public String Description { get; set; }

        public String Length { get; set; }

        public List<Review> reviews { get; set; }

        [Display(Name = "Movie ")]
        [Required(ErrorMessage = "Please choose a cover image")]
        public IFormFile CoverImage { get; set; }
    }
}
