﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class News
    {

        [Key]
        public int NewsId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
        
        [Display(Name = "Date")]
        [Required]
        public DateTime PostTime { get; set; } = DateTime.Now;

        public User user { get; set; }

        [Required]
        public string UserId { get; set; }
        
    }
}
