﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class ReviewsSeries
    {
        [Key]
        public int ReviewId { get; set; }

        public int SerieId { get; set; }

        public string UserId { get; set; }

        [Required]
        public String Description { get; set; }

        [Required]
        [Range(1, 10, ErrorMessage = "O valor para o {0} tem que estar entre {1} e {2}")]
        public String Score { get; set; }

        [DefaultValue(0)]
        public int Likes { get; set; }

        [DefaultValue(0)]
        public int Dislike { get; set; }

        public Serie Serie { get; set; }

        public User User { get; set; }
    }
}
