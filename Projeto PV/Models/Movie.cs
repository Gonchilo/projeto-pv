﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }

        [Required(ErrorMessage = "Please put the title of the movie")]
        public String Title { get; set; }

        [Required(ErrorMessage = "Please write the description of the movie")]
        public String Description { get; set; }

        [Required]
        public String Length { get; set; }

        [Required]
        public string Director { get; set; }

        public List<Review> Reviews { get; set; }

        [Display(Name = "Cover")]
        [Required(ErrorMessage = "Please choose a cover image")]
        public byte[] CoverPhoto { get; set; }
    }
}
