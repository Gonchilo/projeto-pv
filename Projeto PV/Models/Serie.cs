﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class Serie
    {
        [Key]
        public int SerieId { get; set; }

        [Required]
        public String Title { get; set; }

        [Required]
        public String Description { get; set; }

        [Required]
        public String Episodes { get; set; }

        [Required]
        public String Seasons { get; set; }

        [Required]
        public string Director { get; set; }

        public List<Review> reviews { get; set; }

        [Display(Name = "Cover")]
        [Required(ErrorMessage = "Please choose a cover image")]
        public byte[] CoverPhoto { get; set; }
    }
}
