﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class User : IdentityUser
    {
        [PersonalData, Required]
        public String Nickname { get; set; }

        public byte[] ProfilePicture { get; set; }
    }
}
