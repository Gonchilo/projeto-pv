﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Models
{
    public class Notification
    {
        [Key]
        public long IdNotification { get; set; }
        public string IdAccount { get; set; }
        public string Description { get; set; }
        public bool ReadNotification { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public DateTime NotificationDate { get; set; }
    }
}
