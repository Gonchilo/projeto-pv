﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projeto_PV.Data;
using Projeto_PV.Models;
using Projeto_PV.ViewModels;

namespace Projeto_PV.Controllers
{
    public class SeriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IWebHostEnvironment _env;

        public SeriesController(ApplicationDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }

        // GET: Series
        /**
         * Este metodo permite receber uma lista com todas as series e na lista podemos ordenar por ordem decrescente.
         */
        public async Task<IActionResult> Index(string sortBy)
        {
            ViewBag.sortTitleParameter = string.IsNullOrEmpty(sortBy) ? "Title desc" : "";
            ViewBag.sortDescriptionParameter = string.IsNullOrEmpty(sortBy) ? "Description desc" : "";
            ViewBag.sortEpisodesParameter = string.IsNullOrEmpty(sortBy) ? "Episodes desc" : "";
            ViewBag.sortSeasonsParameter = string.IsNullOrEmpty(sortBy) ? "Seasons desc" : "";
            ViewBag.sortDirectorParameter = string.IsNullOrEmpty(sortBy) ? "Director desc" : "";

            var series = _context.Serie.AsQueryable();

            switch (sortBy)
            {
                case "Title desc":
                    series = series.OrderByDescending(x => x.Title);
                    break;
                case "Description desc":
                    series = series.OrderBy(x => x.Description);
                    break;
                case "Episodes desc":
                    series = series.OrderByDescending(x => x.Episodes);
                    break;
                case "Seasons desc":
                    series = series.OrderByDescending(x => x.Seasons);
                    break;
                case "Director desc":
                    series = series.OrderByDescending(x => x.Director);
                    break;
                default:
                    series = series.OrderBy(x => x.Title);
                    break;
            }
            return View(series);
        }

        // GET: Series/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serie = await _context.Serie
                .FirstOrDefaultAsync(m => m.SerieId == id);
            if (serie == null)
            {
                return NotFound();
            }

            return View(serie);
        }

        // GET: Series/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Series/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("SerieId,Title,Description,Episodes,Seasons,Director,CoverPhoto")] Serie serie)
        {
            byte[] photo = null;

            if (Request.Form.Files.Count > 0)
            {

                IFormFile file = Request.Form.Files.FirstOrDefault();
                using (var dataStream = new MemoryStream())
                {
                    await file.CopyToAsync(dataStream);
                    photo = dataStream.ToArray();

                }


                Serie serie2 = new Serie
                {
                    Title = serie.Title,
                    Description = serie.Description,
                    Episodes = serie.Episodes,
                    Seasons = serie.Seasons,
                    Director = serie.Director,
                    CoverPhoto = photo
                };

                _context.Add(serie2);
                await _context.SaveChangesAsync();
                CreateNotification("Series", "Index", "The serie " + serie.Title + " was added to the series list!");
                return RedirectToAction(nameof(Index));
            }

            return View(serie);
        }

        // GET: Series/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serie = await _context.Serie.FindAsync(id);
            if (serie == null)
            {
                return NotFound();
            }
            return View(serie);
        }

        // POST: Series/Edit/5
        /**
         * Este metodo retorna a serie com as alterações desejadas pelo administrador
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("SerieId,Title,Description,Episodes,Seasons,Director,CoverPhoto")] Serie serie)
        {
            if (id != serie.SerieId)
            {
                return NotFound();
            }

            if (Request.Form.Files.Count > 0)
            {
                try
                {

                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        await file.CopyToAsync(dataStream);
                        serie.CoverPhoto = dataStream.ToArray();
                    }


                    _context.Update(serie);
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SerieExists(serie.SerieId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                CreateNotification("Series", "Index", "The series " + serie.Title + " has been edited with success");
                return RedirectToAction(nameof(Index));
            }
            return View(serie);
        }

        // GET: Series/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var serie = await _context.Serie
                .FirstOrDefaultAsync(m => m.SerieId == id);
            if (serie == null)
            {
                return NotFound();
            }

            return View(serie);
        }

        // POST: Series/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var serie = await _context.Serie.FindAsync(id);
            _context.Serie.Remove(serie);
            await _context.SaveChangesAsync();
            CreateNotification("Series", "Index", "The series " + serie.Title + " has been deleted from the series list");
            return RedirectToAction(nameof(Index));
        }

        private bool SerieExists(int id)
        {
            return _context.Serie.Any(e => e.SerieId == id);
        }

        private void CreateNotification(string controllername, string actionname, string description)
        {
            Console.WriteLine(controllername + " | " + actionname + " | " + description);

            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            Console.WriteLine(DateTime.Now);

            Notification ntf = new Notification
            {
                ReadNotification = false,
                Description = description,
                ControllerName = controllername,
                ActionName = actionname,
                NotificationDate = DateTime.Now,
                IdAccount = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };
            _context.Notification.Add(ntf);
            _context.SaveChanges();
        }
    }
}
