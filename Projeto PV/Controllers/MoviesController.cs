﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projeto_PV.Data;
using Projeto_PV.Models;
using Projeto_PV.ViewModels;

namespace Projeto_PV.Controllers
{
    public class MoviesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IWebHostEnvironment _env;

        public MoviesController(ApplicationDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }

        // GET: Movies
        /**
         * Este metodo permite receber uma lista com todos os filmes e na lista podemos ordenar por ordem decrescente.
         */
        public async Task<IActionResult> Index(string sortBy)
        {
            ViewBag.sortTitleParameter = string.IsNullOrEmpty(sortBy) ? "Title desc" : "";
            ViewBag.sortDescriptionParameter = string.IsNullOrEmpty(sortBy) ? "Description desc" : "";
            ViewBag.sortLengthParameter = string.IsNullOrEmpty(sortBy) ? "Length desc" : "";
            ViewBag.sortDirectorParameter = string.IsNullOrEmpty(sortBy) ? "Director desc" : "";
            ViewBag.number = 1;


            var movies = _context.Movie.AsQueryable();

            switch (sortBy)
            {
                case "Title desc":
                    movies = movies.OrderByDescending(x => x.Title);
                    break;
                case "Description desc":
                    movies = movies.OrderBy(x => x.Description);
                    break;
                case "Length desc":
                    movies = movies.OrderByDescending(x => x.Length);
                    break;
                case "Director desc":
                    movies = movies.OrderByDescending(x => x.Director);
                    break;
                default:
                    movies = movies.OrderBy(x => x.Title);
                    break;
            }
            return View(movies);
        }

        // GET: Movies/Details/5
        /**
        * Este metodo permite apresenta a descrição do filme como os restantes campos.
        */
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie
                .FirstOrDefaultAsync(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            return View();
        }


        // POST: Movies/Create
        /**
        * Este metodo cria o filme recebendo todos os dados pelo administrador. Cria uma notificação a dizer que X filme foi adicionado.
        */
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("MovieId,Title,Description,Length,Director,CoverPhoto")] Movie movie)
        {
            byte[] photo = null;

            if (Request.Form.Files.Count > 0)
            {

                IFormFile file = Request.Form.Files.FirstOrDefault();
                using (var dataStream = new MemoryStream())
                {
                    await file.CopyToAsync(dataStream);
                    photo = dataStream.ToArray();

                }

                Movie movie2 = new Movie
                {
                    Title = movie.Title,
                    Description = movie.Description,
                    Length = movie.Length,
                    Director = movie.Director,
                    CoverPhoto = photo,
                };

                _context.Add(movie2);
                CreateNotification("Movies", "Index", "The movie " + movie.Title + " was added to the movie list!");
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(movie);
        }

        // GET: Movies/Edit/5

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // POST: Movies/Edit/5
        /**
         * Este metodo retorna o filme com as alterações desejadas pelo administrador
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("MovieId,Title,Description,Length,Director,CoverPhoto")] Movie movie)
        {
            if (id != movie.MovieId)
            {
                return NotFound();
            }

            //if (ModelState.IsValid)
            //{
            if (Request.Form.Files.Count > 0)
            {
                try
                {                    
                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        await file.CopyToAsync(dataStream);
                        movie.CoverPhoto = dataStream.ToArray();
                    }


                    _context.Update(movie);
                    await _context.SaveChangesAsync();
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(movie.MovieId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                CreateNotification("Movies", "Index", "The movie " + movie.Title + " has been edited with success");
                return RedirectToAction(nameof(Index));
            }

            return View(movie);

        }

        // GET: Movies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movie
                .FirstOrDefaultAsync(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();
            CreateNotification("Movies", "Index", "The movie " + movie.Title + " has been deleted from the movie list");
            return RedirectToAction(nameof(Index));
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }

        /**
         * Cria uma notificação e redireciona para o controlador e ação desejada. 
         */
        private void CreateNotification(string controllername, string actionname, string description)
        {           
          
            Notification ntf = new Notification
            {
                ReadNotification = false,
                Description = description,
                ControllerName = controllername,
                ActionName = actionname,
                NotificationDate = DateTime.Now,
                IdAccount = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };
            _context.Notification.Add(ntf);
            _context.SaveChanges();
        }
    }
}