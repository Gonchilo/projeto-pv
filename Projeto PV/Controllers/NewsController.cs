﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projeto_PV.Data;
using Projeto_PV.Models;
using Microsoft.AspNetCore.Identity;

namespace Projeto_PV.Controllers
{
    public class NewsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public NewsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: News
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.News.Include(r => r.user).AsQueryable();
            return View(await applicationDbContext.ToListAsync());
        }       

        // GET: News/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NewsId,Title,Description,PostTime,UserId")] News news)
        {
            if (!ModelState.IsValid)
            {
                news.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                news.PostTime = DateTime.Now;
                _context.Add(news);
                await _context.SaveChangesAsync();
                CreateNotification("News", "Index", "The news " + news.Title + " was added to the news list!");

                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", news.UserId);
            return View(news);
        }

        // GET: News/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", news.UserId);
            return View(news);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NewsId,Title,Description,PostTime,UserId")] News news)
        {
            if (id != news.NewsId)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                try
                {
                    news.PostTime = DateTime.Now;
                    news.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    _context.Update(news);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewsExists(news.NewsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                CreateNotification("News", "Index", "The news " + news.Title + " has been edited with success");
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", news.UserId);
            return View(news);
        }

        // GET: News/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News
                .FirstOrDefaultAsync(m => m.NewsId == id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var news = await _context.News.FindAsync(id);
            _context.News.Remove(news);
            await _context.SaveChangesAsync();
            CreateNotification("News", "Index", "The news " + news.Title + " has been deleted from the news list");
            return RedirectToAction(nameof(Index));
        }

        private bool NewsExists(int id)
        {
            return _context.News.Any(e => e.NewsId == id);
        }

        private void CreateNotification(string controllername, string actionname, string description)
        {

            Notification ntf = new Notification
            {
                ReadNotification = false,
                Description = description,
                ControllerName = controllername,
                ActionName = actionname,
                NotificationDate = DateTime.Now,
                IdAccount = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };
            _context.Notification.Add(ntf);
            _context.SaveChanges();
        }
    }
}
