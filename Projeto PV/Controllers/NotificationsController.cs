﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Projeto_PV.Data;
using Projeto_PV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Projeto_PV.Controllers
{
    public class NotificationsController : Controller
    {
        //Retorna o número de notificações não lidas pelo utilizador
        public int NotificationsCount(ClaimsPrincipal user)
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM Notification where IdAccount = @AccountId AND ReadNotification = 0";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                scmCommand.Parameters.AddWithValue("@AccountId", GetCurrentUserID(user));
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o id do utilizador que está logado
        public string GetCurrentUserID(ClaimsPrincipal user)
        {
            return user.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        //Verifica se as notificações já foram lidas pelo utilizador
        [HttpPost]
        public ActionResult ReadNotifications()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "UPDATE Notification SET ReadNotification = 1 where IdAccount = @AccountId";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                scmCommand.Parameters.AddWithValue("@AccountId", GetCurrentUserID(User));

                scmCommand.ExecuteNonQuery();
            }

            return PartialView("~/Views/Shared/_Notifications.cshtml", NotificationsCount(User).ToString());
        }

        //Retorna as notificações
        public List<Notification> GetNotifications(ClaimsPrincipal user)
        {
            List<Notification> list = new List<Notification>();
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "Select top 5 * FROM Notification where IdAccount = @AccountId order by IdNotification desc";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                scmCommand.Parameters.AddWithValue("@AccountId", GetCurrentUserID(user));

                Notification aux = null;

                SqlDataReader reader = scmCommand.ExecuteReader();

                while (reader.Read())
                {                
                    aux = new Notification { IdNotification = (long)reader[0], IdAccount = (string)reader[1], Description = (string)reader[2], ReadNotification = (bool)reader[3], ControllerName = (string)reader[4], ActionName = (string)reader[5], NotificationDate = (DateTime)reader[6] };
                    list.Add(aux);
                }
            }

            return list;
        }

        
    }
}
