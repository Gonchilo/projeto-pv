﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projeto_PV.Data;
using Projeto_PV.Models;

namespace Projeto_PV.Controllers
{
    public class ReviewsSeriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReviewsSeriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ReviewsSeries
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ReviewsSeries.Include(r => r.Serie).Include(r => r.User);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ReviewsSeries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reviewsSeries = await _context.ReviewsSeries
                .Include(r => r.Serie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.ReviewId == id);
            if (reviewsSeries == null)
            {
                return NotFound();
            }

            return View(reviewsSeries);
        }

        // GET: ReviewsSeries/Create
        public IActionResult Create()
        {
            ViewData["SerieId"] = new SelectList(_context.Serie, "SerieId", "Title");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: ReviewsSeries/Create
        /**
         * É criado o review para a serie e atribuido aqui o utilizador que a criou
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ReviewId,SerieId,UserId,Description,Score,Likes,Dislike")] ReviewsSeries reviewsSeries)
        {
            if (ModelState.IsValid)
            {
                ReviewsSeries review2 = new ReviewsSeries
                {
                    SerieId = reviewsSeries.SerieId,
                    Description = reviewsSeries.Description,
                    Score = reviewsSeries.Score,
                    UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                };

                _context.Add(review2);
                await _context.SaveChangesAsync();
                CreateNotification("ReviewsSeries", "Index", "A new series review with a score of " + reviewsSeries.Score + " was added to the series reviews list");
                return RedirectToAction(nameof(Index));
            }
            ViewData["SerieId"] = new SelectList(_context.Serie, "SerieId", "SerieId", reviewsSeries.SerieId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", reviewsSeries.UserId);
            return View(reviewsSeries);
        }

        // GET: ReviewsSeries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reviewsSeries = await _context.ReviewsSeries.FindAsync(id);
            if (reviewsSeries == null)
            {
                return NotFound();
            }
            ViewData["SerieId"] = new SelectList(_context.Serie, "SerieId", "SerieId", reviewsSeries.SerieId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", reviewsSeries.UserId);
            return View(reviewsSeries);
        }

        // POST: ReviewsSeries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ReviewId,SerieId,UserId,Description,Score,Likes,Dislike")] ReviewsSeries reviewsSeries)
        {
            if (id != reviewsSeries.ReviewId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    reviewsSeries.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    _context.Update(reviewsSeries);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReviewsSeriesExists(reviewsSeries.ReviewId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                CreateNotification("ReviewsSeries", "Index", "The series review has been edited with success");
                return RedirectToAction(nameof(Index));
            }
            ViewData["SerieId"] = new SelectList(_context.Serie, "SerieId", "SerieId", reviewsSeries.SerieId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", reviewsSeries.UserId);
            return View(reviewsSeries);
        }

        // GET: ReviewsSeries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reviewsSeries = await _context.ReviewsSeries
                .Include(r => r.Serie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.ReviewId == id);
            if (reviewsSeries == null)
            {
                return NotFound();
            }

            return View(reviewsSeries);
        }

        // POST: ReviewsSeries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reviewsSeries = await _context.ReviewsSeries.FindAsync(id);
            _context.ReviewsSeries.Remove(reviewsSeries);
            await _context.SaveChangesAsync();
            CreateNotification("ReviewsSeries", "Index", "The series review has been deleted from the series review list");
            return RedirectToAction(nameof(Index));
        }

        // Verifica se a review existe na base de dados retornando true se sim
        private bool ReviewsSeriesExists(int id)
        {
            return _context.ReviewsSeries.Any(e => e.ReviewId == id);
        }

        // Retorna o nome da serie pelo url
        private string getSeriesNameByUrl()
        {
            string s = Request.QueryString.ToString();
            string aux = null;
            string[] subs = s.Split('=');
            for (int i = 0; i < subs.Length; i++)
            {
                aux = subs[i];
            }

            return aux;
        }

        // Retorna todos as reviews de uma certa serie e permite que estas sejam ordenadas clickando no nome do critério de ordenação.
        public async Task<IActionResult> ReviewsBySerie(string sortBy, string serie)
        {
            ViewBag.sortScoreParameter = string.IsNullOrEmpty(sortBy) ? "Score desc" : "";
            ViewBag.sortLikesParameter = string.IsNullOrEmpty(sortBy) ? "Likes desc" : "";
            ViewBag.sortDislikeParameter = string.IsNullOrEmpty(sortBy) ? "Dislike desc" : "";

            TempData["fullpath"] = getSeriesNameByUrl();

            var context = _context.ReviewsSeries.Include(r => r.Serie).Include(r => r.User).AsQueryable();

            switch (sortBy)
            {
                case "Score desc":
                    context = context.OrderByDescending(x => x.Score);
                    break;
                case "Likes desc":
                    context = context.OrderByDescending(x => x.Likes);
                    break;
                case "Dislike desc":
                    context = context.OrderByDescending(x => x.Dislike);
                    break;
                default:
                    context = context.OrderBy(x => x.Likes);
                    break;
            }

            return View("Index", await context.Where(r => r.Serie.Title == serie).ToListAsync());
        }

        // Retorna as reviews de um certo utilizador
        public async Task<IActionResult> ReviewByUser()
        {
            var context = _context.ReviewsSeries.Include(r => r.Serie).Include(r => r.User);
            return View("Index", await context.Where(r => r.UserId == User.FindFirst(ClaimTypes.NameIdentifier).Value).ToListAsync());
        }

        // Cria uma notificação
        private void CreateNotification(string controllername, string actionname, string description)
        {
            Console.WriteLine(controllername + " | " + actionname + " | " + description);

            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            Console.WriteLine(DateTime.Now);

            Notification ntf = new Notification
            {
                ReadNotification = false,
                Description = description,
                ControllerName = controllername,
                ActionName = actionname,
                NotificationDate = DateTime.Now,
                IdAccount = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };
            _context.Notification.Add(ntf);
            _context.SaveChanges();
        }
    }
}
