﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Projeto_PV.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Projeto_PV.Controllers
{
    public class Statistics : Controller
    {

        public Statistics()
        {
            
        }

        public IActionResult Index()
        {
            return View();
        }

        //Retorna o número de filmes
        public int GetNumberOfFilms()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM Movie";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o número de séries
        public int GetNumberOfSeries()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM Serie";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o número de utilizadores
        public int GetNumberOfUsers()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM AspNetUsers";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o número de reviews por filme
        public int GetNumberOfReviewsOfMovies()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM Review";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o número de reviews por série
        public int GetNumberOfReviewsOfSeries()
        {
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT COUNT(*) FROM ReviewsSeries";

                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        return (int)dtrReader[0];
                    }
                }
            }

            return 0;
        }

        //Retorna o número de reviews e o nome do filme que tem mais reviews
        public List<string> GetNumberOfMostReviewInMovie()
        {
            List<string> list = new List<string>();
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT TOP 1 COUNT(*), Title FROM Review as r, Movie as m WHERE r.MovieId = m.MovieId GROUP BY Title ORDER BY COUNT(r.MovieId) DESC";


                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        list.Add(dtrReader[0].ToString());
                        list.Add((string)dtrReader[1]);
                        return list;
                    }
                }
            }

            return list;
        }

        //Retorna o número de reviews e o nome da séries que tem mais reviews
        public List<string> GetNumberOfMostReviewInSerie()
        {
            List<string> list = new List<string>();
            using (SqlConnection scnConnection = new SqlConnection(ApplicationDbContext.ConnectionString))
            {
                scnConnection.Open();
                string strQuery = "SELECT TOP 1 COUNT(*), Title FROM ReviewsSeries as r, Serie as m WHERE r.SerieId = m.SerieId GROUP BY Title ORDER BY COUNT(r.SerieId) DESC";


                SqlCommand scmCommand = new SqlCommand(strQuery, scnConnection);
                SqlDataReader dtrReader = scmCommand.ExecuteReader();
                if (dtrReader.HasRows)
                {
                    while (dtrReader.Read())
                    {
                        list.Add(dtrReader[0].ToString());
                        list.Add((string)dtrReader[1]);
                        return list;
                    }
                }
            }

            return list;
        }


    }
}
