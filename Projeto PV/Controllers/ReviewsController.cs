﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projeto_PV.Data;
using Projeto_PV.Models;

namespace Projeto_PV.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReviewsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Reviews
        public async Task<IActionResult> Index(string sortBy)
        {

            var applicationDbContext = _context.Review.Include(r => r.Movie).Include(r => r.User).AsQueryable();

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Reviews/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review
                .Include(r => r.Movie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.ReviewId == id);
            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // GET: Reviews/Create
        public IActionResult Create()
        {
            ViewData["MovieId"] = new SelectList(_context.Movie, "MovieId", "Title");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ReviewId,MovieId,UserId,Description,Score,Likes,Dislike")] Review review)
        {
            if (ModelState.IsValid)
            {
                Review review2 = new Review
                {
                    MovieId = review.MovieId,
                    Description = review.Description,
                    Score = review.Score,
                    UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                };                

                _context.Add(review2);
                await _context.SaveChangesAsync();
                CreateNotification("Reviews", "Index", "A new movie review with a score of " + review.Score + " was added to the movie reviews list");
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movie, "MovieId", "Title", review.MovieId);            
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", review.UserId);
            return View(review);
        }

        // GET: Reviews/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }
            ViewData["MovieId"] = new SelectList(_context.Movie, "MovieId", "Title", review.MovieId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", review.UserId);
            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("ReviewId,MovieId,UserId,Description,Score,Likes,Dislike")] Review review)
        {
            if (id != review.ReviewId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Review review2 = new Review
                    {
                        MovieId = review.MovieId,
                        Description = review.Description,
                        Score = review.Score,
                        UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                    };
                    _context.Update(review2);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReviewExists(review.ReviewId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                CreateNotification("Reviews", "Index", "The movie review has been edited with success");
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movie, "MovieId", "Title", review.MovieId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", review.UserId);
            return View(review);
        }

        // GET: Reviews/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review
                .Include(r => r.Movie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.ReviewId == id);
            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var review = await _context.Review.FindAsync(id);
            _context.Review.Remove(review);
            await _context.SaveChangesAsync();
            CreateNotification("Reviews", "Index", "The movie review has been deleted from the movie reviews list");
            return RedirectToAction(nameof(Index));
        }

        private bool ReviewExists(int id)
        {
            return _context.Review.Any(e => e.ReviewId == id);
        }

        private string getMovieNameByUrl()
        {
            string s = Request.QueryString.ToString();
            string aux = null;
            string[] subs = s.Split('=');
            for (int i = 0; i < subs.Length; i++)
            {
                aux = subs[i];
            }

            return aux;
        }

        public async Task<IActionResult> ReviewsByMovie(string sortBy, string movie)
        {
            ViewBag.sortDescriptionParameter = string.IsNullOrEmpty(sortBy) ? "Description desc" : "";
            ViewBag.sortScoreParameter = string.IsNullOrEmpty(sortBy) ? "Score desc" : "";
            ViewBag.sortLikesParameter = string.IsNullOrEmpty(sortBy) ? "Likes desc" : "";
            ViewBag.sortDislikeParameter = string.IsNullOrEmpty(sortBy) ? "Dislike desc" : "";
            ViewBag.sortMovieParameter = string.IsNullOrEmpty(sortBy) ? "Movie desc" : "";
            ViewBag.sortUserParameter = string.IsNullOrEmpty(sortBy) ? "User desc" : "";

            TempData["fullpath"] = getMovieNameByUrl();
            
            var context = _context.Review.Include(r => r.Movie).Include(r => r.User).AsQueryable();

            switch (sortBy)
            {
                case "Description desc":
                    context = context.OrderByDescending(x => x.Description);
                    break;
                case "Score desc":
                    context = context.OrderByDescending(x => x.Score);
                    break;
                case "Likes desc":
                    context = context.OrderByDescending(x => x.Likes);
                    break;
                case "Dislike desc":
                    context = context.OrderByDescending(x => x.Dislike);
                    break;
                case "Movie desc":
                    context = context.OrderByDescending(x => x.Movie);
                    break;
                case "User desc":
                    context = context.OrderByDescending(x => x.User);
                    break;
                default:
                    context = context.OrderBy(x => x.Description);
                    break;
            }

            return View("Index", await context.Where(r => r.Movie.Title == movie).ToListAsync());
        }

        
        public async Task<IActionResult> Incremment(int id)//, int movieid)
        {
                var review = await _context.Review.FindAsync(id);
                review.Likes++;
                //users.Add(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                _context.Update(review);
                await _context.SaveChangesAsync();

            var context = _context.Review.Include(r => r.Movie).Include(r => r.User);
            return View("Index", await context.Where(r => r.MovieId == 1).ToListAsync());
        }


        public async Task<IActionResult> ReviewByUser()
        {
            var context = _context.Review.Include(r => r.Movie).Include(r => r.User);
            return View("Index", await context.Where(r => r.UserId == User.FindFirst(ClaimTypes.NameIdentifier).Value).ToListAsync());
        }

        private void CreateNotification(string controllername, string actionname, string description)
        {
            Console.WriteLine(controllername + " | " + actionname + " | " + description);

            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            Console.WriteLine(DateTime.Now);

            Notification ntf = new Notification
            {
                ReadNotification = false,
                Description = description,
                ControllerName = controllername,
                ActionName = actionname,
                NotificationDate = DateTime.Now,
                IdAccount = User.FindFirst(ClaimTypes.NameIdentifier).Value
            };
            _context.Notification.Add(ntf);
            _context.SaveChanges();
        }

    }
}