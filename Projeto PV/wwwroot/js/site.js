﻿function toogleNotifications() {

    if (document.getElementById("submenu-notifications").style.display !== "none") {
        document.getElementById("submenu-notifications").style.display = "none";
        $("#submenu-settings").slideToggle(250);
    } else {
        document.getElementById("submenu-notifications").style.display = "block";
        $("#submenu-settings").slideToggle(250);
    }

    if (document.getElementById("submenu-notifications").style.display !== "block") {
        var actionUrl = '/Notifications/ReadNotifications';

        $.ajax({
            type: "POST",
            url: actionUrl,
            contentType: "application/json; charset=utf-8",
            dataType: "html",
        }).done(function (res) {
            $("#notificationsCount").html("");
        });;

    }

    //document.getElementById("submenu-settings").style.display = "none";
    //$("#submenu-notifications").slideToggle(250);
}

function hideMenus() {
    if (document.getElementById("submenu-settings") !== null)
        document.getElementById("submenu-settings").style.display = "none";

    if (document.getElementById("submenu-notifications") !== null)
        document.getElementById("submenu-notifications").style.display = "none";
}
