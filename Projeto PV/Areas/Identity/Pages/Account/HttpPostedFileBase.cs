﻿using System.IO;

namespace Projeto_PV.Areas.Identity.Pages.Account
{
    internal class HttpPostedFileBase
    {
        public Stream InputStream { get; internal set; }
        public int ContentLength { get; internal set; }
    }
}