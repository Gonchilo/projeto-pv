#pragma checksum "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "65ac24f09205de2a0ce7bc9dcf4c15f239e38bc5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Statistics_Index), @"mvc.1.0.view", @"/Views/Statistics/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\_ViewImports.cshtml"
using Projeto_PV;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\_ViewImports.cshtml"
using Projeto_PV.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\_ViewImports.cshtml"
using Projeto_PV.Controllers;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"65ac24f09205de2a0ce7bc9dcf4c15f239e38bc5", @"/Views/Statistics/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5777648f7972016764845e9e11aa98cdc018dc61", @"/Views/_ViewImports.cshtml")]
    public class Views_Statistics_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
   Statistics st = new Statistics(); 

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Statistics</h1>\r\n<br />\r\n<div class=\"row\">\r\n    <div class=\"column\">\r\n        <canvas id=\"watched\"></canvas>\r\n        <div style=\"margin-left:45px; padding: 10px;\">\r\n            <h5 style=\"color:#6495ED\">Number of Movies: <b>");
#nullable restore
#line 13 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                      Write(st.GetNumberOfFilms());

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></h5>\r\n            <h5 style=\"color:#228B22\">Number of Series: <b>");
#nullable restore
#line 14 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                      Write(st.GetNumberOfSeries());

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></h5>\r\n        </div>\r\n    </div>\r\n    <div class=\"column\">\r\n        <canvas id=\"reviews\"></canvas>\r\n        <div style=\"margin: auto; padding: 10px;\">\r\n            <h5 style=\"color:#6495ED\">Number of Movie Reviews: <b>");
#nullable restore
#line 20 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                             Write(st.GetNumberOfReviewsOfMovies());

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></h5>\r\n            <h5 style=\"color:#228B22\">Number of Series Reviews: <b>");
#nullable restore
#line 21 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                              Write(st.GetNumberOfReviewsOfSeries());

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></h5>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<br />\r\n<h5>Number of registered users: <b style=\"color:#ff0000\">");
#nullable restore
#line 27 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                    Write(st.GetNumberOfUsers());

#line default
#line hidden
#nullable disable
            WriteLiteral("</b></h5>\r\n\r\n");
            WriteLiteral("    <br />\r\n");
#nullable restore
#line 31 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
    var sta = st.GetNumberOfMostReviewInMovie();

#line default
#line hidden
#nullable disable
            WriteLiteral("    <h5 >Movie with highest number of reviews: <b >");
#nullable restore
#line 32 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                              Write(sta[1]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b> with <b style=\"color:#6495ED\">");
#nullable restore
#line 32 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                                                        Write(sta[0]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b> reviews</h5>\r\n");
            WriteLiteral("\r\n");
            WriteLiteral("    <br />\r\n");
#nullable restore
#line 37 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
    var sta2 = st.GetNumberOfMostReviewInSerie();

#line default
#line hidden
#nullable disable
            WriteLiteral("    <h5>Serie with highest number of reviews: <b>");
#nullable restore
#line 38 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                            Write(sta2[1]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b> with <b style=\"color:#228B22\">");
#nullable restore
#line 38 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                                                       Write(sta2[0]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b> reviews</h5>\r\n");
            WriteLiteral("\r\n\r\n\r\n\r\n<script>\r\n    var ctx = document.getElementById(\'watched\');\r\n    var watched = new Chart(ctx, {\r\n    type: \'doughnut\',\r\n    data: {\r\n        labels: [\'Movies\', \'Series\'],\r\n        datasets: [{\r\n            label: \'# of Votes\',\r\n            data: [");
#nullable restore
#line 52 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
              Write(st.GetNumberOfFilms());

#line default
#line hidden
#nullable disable
            WriteLiteral(", ");
#nullable restore
#line 52 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                      Write(st.GetNumberOfSeries());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"],
            backgroundColor: [
                '#6495ED',
                '#228B22',
            ],
            borderColor: [
                '#6495ED',
                '#228B22',
            ],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true
    }
    });

    var ctx2 = document.getElementById('reviews');
    var watched = new Chart(ctx2, {
    type: 'doughnut',
    data: {
        labels: ['Movie Reviews', 'Series Reviews'],
        datasets: [{
            label: '# of Votes',
            data: [");
#nullable restore
#line 76 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
              Write(st.GetNumberOfReviewsOfMovies());

#line default
#line hidden
#nullable disable
            WriteLiteral(", ");
#nullable restore
#line 76 "C:\Users\Goncalo\Desktop\Projeto PV2\Projeto PV\Views\Statistics\Index.cshtml"
                                                Write(st.GetNumberOfReviewsOfSeries());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"],
            backgroundColor: [
                '#6495ED',
                '#228B22',
            ],
            borderColor: [
                '#6495ED',
                '#228B22',
            ],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true
    }
});
</script>

<style>
    .column {
        float: left;
        width: 33.33%;
        padding: 5px;
    }

    .row::after {
        content: """";
        clear: both;
        display: table;
    }
</style>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
