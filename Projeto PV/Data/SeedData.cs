﻿using Microsoft.AspNetCore.Identity;
using Projeto_PV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_PV.Data
{
    public class SeedData
    {
        public static async Task Seed(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            await SeedRolesAsync(roleManager);
            await SeedUsersAsync(userManager);
        }

        private static async Task SeedUsersAsync(UserManager<User> userManager)
        {

            if (userManager.FindByNameAsync("Admin").Result == null)
            {
                var admin = new User { Nickname = "Admin", PhoneNumber = "123456789", UserName = "admin@ips.pt", Email = "admin@ips.pt" };
                var result = await userManager.CreateAsync(admin, "123456");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "Admin");
                }
            }

            if (userManager.FindByNameAsync("João Manuel").Result == null)
            {
                var joao = new User { Nickname = "João Manuel", PhoneNumber = "1234567890", UserName = "jonas@ip.pt", Email = "jonas@ip.pt" };
                var result1 = await userManager.CreateAsync(joao, "123456");
                if (result1.Succeeded)
                {
                    await userManager.AddToRoleAsync(joao, "User");
                }
            }

            if (userManager.FindByNameAsync("Ana").Result == null)
            {
                var ana = new User { Nickname = "Ana", PhoneNumber = "234567890", UserName = "ana@gmail.com", Email = "ana@gmail.com" };
                var result2 = await userManager.CreateAsync(ana, "123456");
                if (result2.Succeeded)
                {
                    await userManager.AddToRoleAsync(ana, "User");
                }
            }
        }

        private static async Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var usersRole = new IdentityRole("User");
            if (!await roleManager.RoleExistsAsync(usersRole.Name))
            {
                await roleManager.CreateAsync(usersRole);
            }

            var adminsRole = new IdentityRole("Admin");
            if (!await roleManager.RoleExistsAsync(adminsRole.Name))
            {
                await roleManager.CreateAsync(adminsRole);
            }
        }
    }
}
