﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Projeto_PV.Models;

namespace Projeto_PV.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public static string ConnectionString { get; internal set; }
        public DbSet<Projeto_PV.Models.Serie> Serie { get; set; }
        public DbSet<Projeto_PV.Models.Movie> Movie { get; set; }
        public DbSet<Projeto_PV.Models.Review> Review { get; set; }
        public DbSet<Projeto_PV.Models.ReviewsSeries> ReviewSeries { get; set; }
        public DbSet<Projeto_PV.Models.Notification> Notification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        public DbSet<Projeto_PV.Models.News> News { get; set; }

        public DbSet<Projeto_PV.Models.ReviewsSeries> ReviewsSeries { get; set; }
    }
}
